# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import os


def create_path(_path):
    """
    Checks if a folder exists and, if not, creates the folder.
    :param _path: String containing a (relative) folder path, e.g. 'output/A4_data_set'
    :return: None
    """
    if not os.path.isdir(_path):
        path_segments = _path.split(os.path.sep)
        current_path = ""
        first_item = True
        for item in path_segments:
            if not first_item:
                current_path += os.path.sep
            current_path += item
            first_item = False
            if not os.path.isdir(current_path):
                os.makedirs(current_path)
    pass


def assemble_path(_origin_path, _path_segments):
    """
    Joins a list of path elements by adding the system separator.
    :param _origin_path: String containing the path to the origin from where _path_segments start, e.g. 'D:'.
    :param _path_segments: List of strings each containing a single path element (e.g. drive letter or folder), e.g.
    'a4_dataset/run78550'.
    :return: String containing the assembled path, e.g. 'D:/a4_dataset/run78550'.
    """
    target_path = _origin_path
    first_segment = True
    for segment in _path_segments:
        target_path = os.path.join(target_path, segment)
        if first_segment:
            # add additional separator if drive letter (e.g. 'C:')
            if segment.endswith(":"):
                target_path = os.path.join(target_path, os.path.sep)
            first_segment = False

    return target_path


def find_file(folder_path, filename):
    """
    Checks if a file in a given path exists and returns True or False.
    :param folder_path: String containing the path to a folder, e.g. 'D:/A4-metadata/input_exampleData'.
    :param filename: String containing the name of a file, e.g. 'logbook_whitelist.json'.
    :return: Boolean that is 'True' if the file in the folder exists; otherwise 'False'.
    """
    file_found = False
    for file in os.listdir(folder_path):
        if file.startswith(filename):
            file_found = True
            break
    return file_found


def file2string(folder_path, filename):
    """
    Reads the content of a file as a string.
    :param folder_path: String containing the path to a folder, e.g. 'D:/A4-metadata/input_exampleData'.
    :param filename: String containing the name of a file, e.g. 'logbook_whitelist.json'.
    :return: String containing the content of the file.
    """
    text_string = ""
    if find_file(folder_path, filename):
        # errors='ignore' suppresses errors due to strange characters (such as °) but may distort the content
        with open(
            os.path.join(folder_path, filename), "r", encoding="utf-8", errors="ignore"
        ) as f:
            for line in f:
                text_string += line
    return text_string
