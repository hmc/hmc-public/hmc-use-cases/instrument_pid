# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import datetime
import os

import pytz
from lxml import etree as ET

from src.config_dict import ConfigDict


class XMLWriter(object):
    """
    Writes a xml file.
    """

    def __init__(
        self, _root_name, _project_dict, pdf_template=False, _file_id="", _version="1.0"
    ):
        """
        Initializes the basic element of the ET object, i.e. the 'root' level, and assigns the Datacite kernel 4.5
        reference.
        :param _root_name: String containing the name of the root section, e.g. "Dataset".
        :param _project_dict: Dictionary containing content of the config/project_config.json file.
        :param pdf_template: Boolean that is True if the xml file is intended for creation of a pdf file (via an html).
        :param _file_id: String containing the name of the file or a part of it as used in subdata_meta.json for
        identifying that file, e.g. 'info' is used to identify files 'info.78850', 'info.78851', 'info.78852'...
        :param _version: String containing the name of the version; optional, default value is "1.0".
        """
        self.file_id = _file_id
        self.pdf_template = pdf_template

        # remember config/project_config.json file content
        self.project_dict = ConfigDict()
        self.project_dict.read_dict(_project_dict)

        # variable, e.g. to be used in ASCII-to-xml conversion
        self.definitions = ConfigDict()

        # default namespaces
        _nsmap = {}
        self.xml_ns = "http://www.w3.org/XML/1998/namespace"
        # _nsmap["xml"] = self.xml_ns
        self.dc_ns = "http://purl.org/dc/elements/1.1/"
        # _nsmap["dc"] = self.dc_ns

        qname = ET.QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
        self.xml_root = ET.Element(
            _root_name.replace(" ", "_"),
            {
                qname: "https://schema.datacite.org/meta/kernel-4.5/ "
                + "https://schema.datacite.org/meta/kernel-4.5/metadata.xsd"
            },
            nsmap=_nsmap,
        )

        # html creation fails with this attribute
        if not pdf_template:
            self.xml_root.set("xmlns", "http://datacite.org/schema/kernel-4")

        pass

    def write_xml(self, filename, _output_path):
        """
        Writes an ET object as xml file.
        :param filename: String containing the name of the file that should be written, e.g. 'Foersterlog.xml'.
        :param _output_path: String containing the path to the output folder where the file should be written,
        e.g. 'D:/a4-metadata/output/A4_data_set/run78850/camac/daten'.
        :return: None.
        """
        # Remove unused namespaces; seems to delete all namespaces (does it search subdirectories of root?)
        # ET.cleanup_namespaces(self.xml_root)

        # Write ET to xml file:
        xml_path = os.path.join(_output_path, filename)
        ET.ElementTree(self.xml_root).write(
            xml_path, encoding="utf-8", xml_declaration=True
        )

        pass

    @staticmethod
    def xml2html(_xml_path, _xsl_path, pdf_creation=False):
        """
        Reads a xml file and converts it into pdf file by applying a formatting provided by a xslt style sheet.
        :param _xml_path: String containing the path to the xml file that should be converted into pdf.
        :param _xsl_path: String containing the path to the xsl file that should be used to format the xml file.
        :param pdf_creation: Boolean which is True if html file is (only) used to create a pdf; default is False.
        :return: String containing the name of the produced html file.
        """
        # parse xml to html using style sheet xslt
        xslt_doc = ET.parse(_xsl_path)
        xslt_transformer = ET.XSLT(xslt_doc)

        source_doc = ET.parse(_xml_path)
        output_doc = xslt_transformer(source_doc)

        if not pdf_creation:
            output_html_path = _xml_path.replace("_template.xml", ".html")
        else:
            output_html_path = _xml_path[:-4] + ".html"
        output_doc.write(output_html_path, pretty_print=True)

        pass

    def add_element(self, _sub_element, _key, _dict, _element_name=""):
        """
        Adds an element to the Etree (xml) handle if the corresponding key (_key) exists in the dictionary (_dict). If
        an (optional) element name is defined, the element's name may differ from the key's name, otherwise, the key
        name is used to name the element.
        :param _sub_element: Python handle of a sub-element of the Etree object.
        :param _key: String containing the key of the dictionary _dict to be read, e.g. 'units'; the key is also used to
        name the added xml element no _element_name is provided.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs providing
        information about the element to be added (e.g. about a person or a file).
        :param _element_name: String containing the name of the element; optional. If none is given the key name is used
        to name the element.
        :return: None type object.
        """
        # use the key's name if name is not defined explicitly
        if _element_name == "":
            _element_name = _key

        # add element
        if _key in _dict.get_content():

            # only add non-empty elements
            add = False
            _value = _dict.get_value(_key)
            if not isinstance(_value, list):
                if not _value.strip() == "":
                    add = True
            else:
                if len(_value) > 0:
                    add = True

            if add:
                # check if definitions for the element are given
                def_found = False

                if len(_value) > 1:
                    # use case-insensitive comparison if string consists of more than 1 char
                    if _value.lower() in self.definitions.get_lowercase_keys():
                        def_found = True
                else:
                    # use case-sensitive comparison if string consists of 1 character, e.g. to distinguish voltage (V)
                    # and velocity (v)
                    if _value in self.definitions.get_keys():
                        def_found = True

                if def_found:
                    # add definition and content if definitions are found
                    _key_def_dict = ConfigDict()
                    _key_def_dict.read_dict(self.definitions.get_value(_value))
                    _sub_element = self.add_defined_element(
                        _sub_element, _element_name, _value, _dict, _key_def_dict
                    )
                else:
                    # otherwise, add content and make report entry (if report handle exist)
                    _sub_element = self.add_value(_sub_element, _element_name, _value)

        return _sub_element

    def add_defined_element(
        self, _sub_element, _element_name, _text_value, _dict, _key_def_dict
    ):
        """
        Adds an element with additional annotations derived from the "definitions" made in project_meta.json,
        subdata_meta.json, definition_global.json, and definition[filename].json, e.g. including namespace convention
        such as "<units qudt:property="Number" owl:rel="sameAs">counts</units>".
        :param _sub_element: Python handle of a sub-element of the Etree object.
        :param _element_name: String containing the name of the element to be created.
        :param _text_value: String containing the content of the element to be created.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs providing
        information about the element to be added (e.g. about a person or a file).
        :param _key_def_dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        of the attributes to be added to the created element (where values are dictionaries, too).
        :return: Handle to the created ET element.
        """
        def_found = False
        element_created = False
        for _key, _dict in _key_def_dict.get_items():
            _value = _dict["value"]

            # create element only once
            if not element_created:
                element = ET.SubElement(_sub_element, _element_name)
                element_created = True

            if _element_name == "units":
                if _key == "typeOf":
                    def_added = self.add_defined_attribute(_key, _value, _dict, element)
                    if def_added:
                        def_found = True

                if _key == "resource":
                    def_added = self.add_defined_attribute(
                        "resource", _value, _dict, element
                    )
                    if def_added:
                        def_found = True

                if _key == "abbreviation":
                    abbrev_handle = self.make_element(element, "abbrev")
                    def_added = self.add_defined_attribute(
                        "property", "abbreviation", _dict, abbrev_handle
                    )
                    if def_added:
                        def_found = True
                    abbrev_handle.set("type", "xsd:string")
                    abbrev_handle.text = _value

                if _key == "rel":
                    rel_handle = self.make_element(element, "rel")
                    def_added = self.add_defined_attribute(
                        "rel", _value, _dict, rel_handle
                    )
                    if def_added:
                        def_found = True

                if _key == "rel_resource":
                    rel_handle = self.make_element(element, "rel")
                    def_added = self.add_defined_attribute(
                        "resource", _value, _dict, rel_handle
                    )
                    if def_added:
                        def_found = True

            else:
                if "namespace" in _dict and "namespace_prefix" in _dict:
                    # add with given namespace
                    # _prefix = _dict["namespace_prefix"]
                    _namespace = _dict["namespace"]
                    if "attr_namespace" in _dict and "attr_namespace_prefix" in _dict:
                        _attr_namespace = _dict["attr_namespace"]
                        element.set(
                            ET.QName(_attr_namespace, _key),
                            ET.QName(_namespace, _value),
                        )
                    else:
                        element.set(_key, ET.QName(_namespace, _value))
                    def_found = True
                else:
                    # add without namespace, e.g. resource with url
                    element.set(_key, _value)

        if def_found:
            sub_name = _sub_element.tag
            _sub_element.set("about", "#" + sub_name)
            element.text = str(_text_value)
        else:
            element = self.add_value(_sub_element, _element_name, _value)

        self.add_properties(element, _element_name)

        return element

    def add_properties(self, element_handle, element_name):
        """
        Adds default properties to default elements such as "name" or "units".
        """
        if element_name == "name":
            element_handle.set("property", ET.QName(self.dc_ns, "title"))
        elif element_name == "comment":
            element_handle.set("property", ET.QName(self.dc_ns, "description"))
        elif element_name == "units":
            element_handle.set(
                "property", ET.QName("http://qudt.org/2.1/vocab/unit#", "units")
            )

        pass

    @staticmethod
    def add_defined_attribute(_key, _value, _dict, element):
        def_added = False
        if "namespace" in _dict and "namespace_prefix" in _dict:
            # add with given namespace
            # _prefix = _dict["namespace_prefix"]
            _namespace = _dict["namespace"]
            if "attr_namespace" in _dict and "attr_namespace_prefix" in _dict:
                _attr_namespace = _dict["attr_namespace"]
                element.set(
                    ET.QName(_attr_namespace, _key), ET.QName(_namespace, _value)
                )
            else:
                element.set(_key, ET.QName(_namespace, _value))
            def_added = True
        else:
            # add without namespace, e.g. resource with url
            element.set(_key, _value)

        return def_added

    @staticmethod
    def make_element(parent_handle, element_name):

        if len(parent_handle.findall(element_name, namespaces=None)) == 0:
            abbrev_handle = ET.SubElement(parent_handle, element_name)
            # print(" ** not found")
        else:
            abbrev_handle = parent_handle.findall(element_name, namespaces=None)[0]
            # print(" ** found")

        return abbrev_handle

    @staticmethod
    def get_type(_value):
        """
        Tries to convert the passed value in various types and, if successful, returns the variable type.
        :param _value: Variable of any type, e.g. string, integer float, numpy array...
        :return: String (containing the determined type of variable).
        """
        # check if _value is numeric
        try:
            _dummy = float(_value)
            if str(_value).count(".") == 1:
                _data_type = "float"
            else:
                _data_type = "integer"
        except:
            _data_type = "string"
            pass

        # check if _value of type string is utc date
        if _data_type == "string":
            if str(_value).count("-") == 3:
                _value = str(_value).rsplit("-", 1)[0]
            elif "+" in str(_value):
                _value = str(_value).rsplit("+", 1)[0]
            format_utc = "%Y-%m-%dT%H:%M:%S"
            try:
                _dummy = datetime.datetime.strptime(str(_value), format_utc)
                _data_type = "datetime"
            except ValueError:
                pass

            format_time = "%H:%M:%S"
            try:
                _dummy = datetime.datetime.strptime(str(_value), format_time)
                _data_type = "time"
            except ValueError:
                pass

        return _data_type

    def add_value(
        self, _et_element, _value_name, _value, _data_type="", create_element=True
    ):
        """
        Adds a value (as text string) as part of a list to a sub-element of an ET object; in contrast to the
        add_array_value() routine, the _data_type is assigned to the added element.
        :param _et_element: Python handle to the sub-element of the ET object.
        :param _value_name: String containing the name of the element that is created to include the _value,
        e.g. "value".
        :param _value: Float or integer to be written as text string to the sub-element, e.g. '1'.
        :param _data_type: String defining the type of column data, e.g. "integer".
        :param create_element: Boolean that is 'True' if a sub-element should be created to contain the value.
        :return: None.
        """
        # try to identify data type of none is given
        if _data_type == "":
            _data_type = self.get_type(_value)

        if create_element:
            _et_element = ET.SubElement(_et_element, _value_name)
        _et_element.text = str(_value)

        return _et_element

    @staticmethod
    def add_attribute(_sub_element, _key, _dict):
        """
        Adds an attribute to a sub-element of an ET object by using the key as the attribute's name.
        :param _sub_element: Python handle to an ET object.
        :param _key: String containing the key (_key) of the dictionary (_dict) whose value should be added;
        the key name defines also the name of the attribute, e.g. 'nameIdentifierScheme'.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs whose
        values should be added.
        :return: None.
        """
        if _key in _dict.get_content():
            _sub_element.set(_key, _dict.get_value(_key))
        pass

    @staticmethod
    def add_renamed_attribute(_sub_element, _key, _dict, _renamed_key):
        """
        Adds an attribute to a sub-element of an ET object by choosing the name of the attribute.
        :param _sub_element: Python handle to an ET object.
        :param _key: String containing the key (_key) of the dictionary (_dict) whose value should be added.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs whose
        values should be added.
        :param _renamed_key: String containing the name of the attribute which should contain the value of _dict[_key];
        the key name may differ from the name of the attribute, e.g. the value of key 'rights_url' is assigned as an
        attribute 'href'.
        :return: None.
        """
        if _key in _dict.get_content():
            _sub_element.set(_renamed_key, _dict.get_value(_key))
        pass

    def add_xml_lang_attribute(self, _element, _language):
        """
        Adds a language attribute using the xml namespace
        :param _element:
        :param _language:
        :return:
        """
        _element.set(ET.QName(self.xml_ns, "lang"), _language)
        pass

    def add_title(self, _dict):
        """
        Adds the title (string text) to the landing page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "title" whose
        value is a string.
        :return: None.
        """
        if "instrument_name" in _dict.get_content():
            _sub_dict = _dict.get_value("instrument_name")
            _instrument_dict = ConfigDict()
            _instrument_dict.read_dict(_sub_dict)

            title = ET.SubElement(self.xml_root, "titles")
            sub_title = self.add_element(
                title, "instrument_name", _instrument_dict, "title"
            )
            if "language" in _instrument_dict.get_content():
                self.add_xml_lang_attribute(
                    sub_title, _instrument_dict.get_value("language")
                )

        pass

    def add_publisher(self, _dict):
        """
        Adds a publisher (string text) to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "publisher" whose
        value is a string.
        :return: None.
        """
        if "publisher" in _dict.get_content():
            _publisher_dict = ConfigDict()
            _publisher_dict.read_dict(_dict.get_value("publisher"))
            if "publisher_name" in _publisher_dict.get_content():
                _publisher = self.add_element(
                    self.xml_root, "publisher_name", _publisher_dict, "publisher"
                )

                if "language" in _publisher_dict.get_content():
                    self.add_xml_lang_attribute(
                        _publisher, _publisher_dict.get_value("language")
                    )

                self.add_orcid_ror(
                    "publisher_ROR",
                    _publisher_dict,
                    _publisher,
                    "publisherIdentifier",
                    "ROR",
                )

        pass

    def add_publication_year(self, _dict):
        """
        Adds the current year as publication year (string text) to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :return: None.
        """
        _dict.add_value("publicationYear", str(datetime.datetime.now().year))
        self.add_element(self.xml_root, "publicationYear", _dict)

        pass

    def add_resource_type(self, _dict):
        """
        Adds the resource type according to the DataCite schema to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :return: None.
        """
        if self.string_found("resourceType", _dict):
            resource = self.add_element(self.xml_root, "resourceType", _dict)
            _sub_dict = {"resourceTypeGeneral": "Instrument"}
            _dummy_dict = ConfigDict()
            _dummy_dict.read_dict(_sub_dict)
            self.add_attribute(resource, "resourceTypeGeneral", _dummy_dict)

        pass

    def add_date(self, _dict):
        """
        Adds the creation date according to the DataCite schema to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :return: None.
        """
        _dict.add_value("date", self.get_utc(datetime.datetime.now()))
        _date = self.add_element(self.xml_root, "date", _dict)
        _dict.add_value("dateType", "Updated")
        self.add_attribute(_date, "dateType", _dict)

        pass

    @staticmethod
    def get_utc(dt):
        """
        Converts a local datetime object into a string representing the UTC time with corresponding timezone shift;
        includes summer and winter time shift.
        :param dt: Datetime object representing a local time.
        :return: String representing UTC time.
        """
        timezone = pytz.timezone("Europe/Berlin")
        timezone_date = timezone.localize(dt, is_dst=None)
        part = str(timezone_date).split("+")
        utc_shift = part[1]
        time = timezone_date.strftime("%Y-%m-%dT%H:%M:%S") + "+" + utc_shift
        return str(time)

    def add_related_item(self, _sub_element, _item_dict):
        """
        Adds information of a relatedItem to an ET object according to the dataCite citation schema
        (https://schema.datacite.org/meta/kernel-4.1/example/datacite-example-full-v4.1.xml).
        :param _sub_element: Python handle to the Etree sub-element where the citation should be added.
        :param _item_dict: Dictionary containing key-value pairs providing information to be added.
        :return: None.
        """
        # according to dataCite schema
        self.add_attribute(_sub_element, "relatedItemType", _item_dict)
        self.add_attribute(_sub_element, "relationType", _item_dict)

        if ("relatedItemIdentifierType" in _item_dict.get_keys()) and (
            "relatedItemIdentifier" in _item_dict.get_keys()
        ):
            related_item_identifier = self.add_element(
                _sub_element, "relatedItemIdentifier", _item_dict
            )
            self.add_attribute(
                related_item_identifier, "relatedItemIdentifierType", _item_dict
            )

        # add titles
        title_list = _item_dict.get_value("titles")
        if len(title_list) > 0:
            titles = ET.SubElement(_sub_element, "titles")
            for _title_text in title_list:
                title = ET.SubElement(titles, "title")
                title.text = str(_title_text)

        self.add_element(_sub_element, "publicationYear", _item_dict)
        self.add_element(_sub_element, "volume", _item_dict)
        self.add_element(_sub_element, "issue", _item_dict)
        self.add_element(_sub_element, "firstPage", _item_dict)

        pass

    def add_related_items_list(self, _dict):
        """
        Adds related items to the README page; if it is the xml template for pdf creation, instrument images
        (if available) are added, as well.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :return: None.
        """
        if (
            "relatedItems" in _dict.get_keys()
            or "instrument_pictures" in _dict.get_keys()
        ):
            related_items = ET.SubElement(self.xml_root, "relatedItems")

            if "relatedItems" in _dict.get_keys():
                for _key, _sub_dict in _dict.get_value("relatedItems").items():
                    item_dict = ConfigDict()
                    item_dict.read_dict(_sub_dict)
                    related_item = ET.SubElement(related_items, "relatedItem")
                    self.add_related_item(related_item, item_dict)
            if self.pdf_template:
                if "instrument_pictures" in _dict.get_keys():
                    # the value tested is a list of list (containing path elements)
                    # create list of image paths and image names (without path)
                    for _list in _dict.get_value("instrument_pictures"):
                        item_dict = ConfigDict()
                        item_dict.add_value("titles", [_list[-1]])
                        item_dict.add_value("relatedItemType", "Image")
                        item_dict.add_value("relationType", "IsDescribedBy")
                        related_item = ET.SubElement(related_items, "relatedItem")
                        self.add_related_item(related_item, item_dict)

        pass

    def add_related_identifier_list(self, _dict):
        """
        Adds related items as relatedIdentifiers (it is good practice to do so because relatedItems are new and older
        algorithms may only look for relatedIdentifiers).
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :return: None.
        """
        if "relatedItems" in _dict.get_keys():
            related_identifiers = ET.SubElement(self.xml_root, "relatedIdentifiers")

            if "relatedItems" in _dict.get_keys():
                for _key, _sub_dict in _dict.get_value("relatedItems").items():
                    item_dict = ConfigDict()
                    item_dict.read_dict(_sub_dict)
                    self.add_related_identifier(related_identifiers, item_dict)

        pass

    def add_related_identifier(self, _sub_element, _item_dict):
        """
        Adds information of a relatedItem to an ET object according to the dataCite citation schema
        (https://schema.datacite.org/meta/kernel-4.1/example/datacite-example-full-v4.1.xml).
        :param _sub_element: Python handle to the Etree sub-element where the citation should be added.
        :param _item_dict: Dictionary containing key-value pairs providing information to be added.
        :return: None.
        """
        # according to dataCite schema
        if (
            "relatedItemIdentifierType" in _item_dict.get_keys()
            and "relatedItemIdentifier" in _item_dict.get_keys()
        ):
            related_item_identifier = self.add_element(
                _sub_element, "relatedItemIdentifier", _item_dict, "relatedIdentifier"
            )
            self.add_renamed_attribute(
                related_item_identifier,
                "relatedItemIdentifierType",
                _item_dict,
                "relatedIdentifierType",
            )
            self.add_attribute(related_item_identifier, "relationType", _item_dict)
            self.add_renamed_attribute(
                related_item_identifier,
                "relatedItemType",
                _item_dict,
                "resourceTypeGeneral",
            )

        pass

    @staticmethod
    def string_found(_key, _dict):
        """
        Checks if a key exists in a dictionary and is assigned to a non-empty string value.
        :param _key: String containing the key that is checked.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs.
        :return: Boolean which is True if the key exists and is a non-empty string.
        """
        was_found = False
        if _key in _dict.get_content():
            _value = _dict.get_value(_key)
            if isinstance(_value, str) and not _value == "":
                was_found = True

        return was_found

    def create_landing_page(self):
        """
        Writes the top-level readme file that summarizes the content of all datasets, e.g. not the data of single
        measurement; for the latter see create_sub_readme().
        :return: None.
        """
        # add DataCite's core elements which are considered to be mandatory
        self.add_datacite_instr_pid(self.project_dict)

        pass

    def add_datacite_instr_pid(self, _dict):
        """
        Adds core elements (if available) of the DataCite schema to the README page.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :return: None.
        """
        # add common elements if they exist
        self.add_identifier(_dict)
        self.add_creator_facilities(_dict)
        self.add_title(_dict)
        self.add_publisher(_dict)
        self.add_publication_year(_dict)
        self.add_subjects(_dict)
        self.add_hosting_facilities(_dict)
        self.add_dates(_dict)
        self.add_resource_type(_dict)
        self.add_related_identifier_list(_dict)
        self.add_description(_dict)
        self.add_related_items_list(_dict)

        pass

    def add_identifier(self, _dict):
        """
        Adds the identifier (string text) minted for THIS instrument to the xml file.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "identifier"
        whose value is a dictionary.
        :return: None.
        """
        if "identifier" in _dict.get_content():
            _id_dict = ConfigDict()
            _id_dict.read_dict(_dict.get_value("identifier"))
            identifier = self.add_element(self.xml_root, "identifier", _id_dict)
            self.add_attribute(identifier, "identifierType", _id_dict)

        pass

    def add_creator_facilities(self, _dict):
        """
        Adds information of the facility hosting the instrument to an ET object according to the dataCite
        schema (https://schema.datacite.org/meta/kernel-4.1/example/datacite-example-full-v4.1.xml).
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        providing information about the facility.
        :return:
        """
        if "creator_facilities" in _dict.get_content():
            creators_dict = ConfigDict()
            creators_dict.read_dict(_dict.get_value("creator_facilities"))

            _creators = ET.SubElement(self.xml_root, "creators")

            for _key, _value_dict in creators_dict.get_items():
                _creator_dict = ConfigDict()
                _creator_dict.read_dict(_value_dict)
                if "facility_name" in _creator_dict.get_content():
                    _creator = ET.SubElement(_creators, "creator")
                    _sub_dict = {"nameType": "Organizational"}
                    _dummy_dict = ConfigDict()
                    _dummy_dict.read_dict(_sub_dict)
                    name = self.add_element(
                        _creator, "facility_name", _creator_dict, "creatorName"
                    )

                    if "language" in _creator_dict.get_content():
                        self.add_xml_lang_attribute(
                            name, _creator_dict.get_value("language")
                        )
                    self.add_attribute(name, "nameType", _dummy_dict)

                    self.add_orcid_ror(
                        "facility_ROR", _creator_dict, _creator, "nameIdentifier", "ROR"
                    )

        pass

    def add_orcid_ror(self, ror_key, _dict, xml_node, identifier_name, id_scheme):
        """
        Adds information of a ROR identifier (https://ror.org/) or an ORCID (https://orcid.org/) to an ET object
        according to the dataCite schema (https://datacite-metadata-schema.readthedocs.io/_/downloads/en/4.5/pdf/).
        :param ror_key: String containing the key name of the ROR identifier in a dictionary-like ConfigDict class,
        e.g. "facility_ROR" or "publisher_ROR".
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        providing information about the facility, publisher, creator...
        :param xml_node: Etree object of the xml node which the ROR belongs to, e.g. the "facilities" xml node.
        :param identifier_name: String containing the name of the identifier according to DataCite which differs, e.g.,
        for facilities, publishers or creators; e.g. "nameIdentifier" or "publisherIdentifier".
        :param id_scheme: String containing the identifier scheme, e.g. "ORCID" or "ROR".
        :return: None. Nodes are directly added to the Etree object.
        """
        if ror_key in _dict.get_content():
            if id_scheme == "ROR":
                # check if ROR contains the ror.org prefix
                _id = _dict.get_value(ror_key)
                if "https://ror.org/" not in _id:
                    _id = "https://ror.org/" + _id

            # choose DataCite names
            add_ror = True
            if identifier_name == "affiliationIdentifier":
                identifier_scheme = "affiliationIdentifierScheme"
                add_as_element = True
            elif identifier_name == "publisherIdentifier":
                identifier_scheme = "publisherIdentifierScheme"
                add_as_element = False
            elif identifier_name == "nameIdentifier":
                identifier_scheme = "nameIdentifierScheme"
                add_as_element = True
            else:
                add_ror = False  # identifier_name doesn't conform to DataCite

            if id_scheme == "ROR":
                id_scheme_url = "https://ror.org/"
            elif id_scheme == "ORCID":
                id_scheme_url = "https://orcid.org/"
            else:
                add_ror = False  # identifier_name doesn't conform to DataCite

            if add_ror:
                _sub_dict = {
                    identifier_name: _id,
                    identifier_scheme: id_scheme,
                    "schemeURI": id_scheme_url,
                }
                _dummy_dict = ConfigDict()
                _dummy_dict.read_dict(_sub_dict)
                if add_as_element:
                    ror_node = self.add_element(xml_node, identifier_name, _dummy_dict)
                else:
                    ror_node = xml_node
                    self.add_attribute(ror_node, identifier_name, _dummy_dict)

                self.add_attribute(ror_node, identifier_scheme, _dummy_dict)
                self.add_attribute(ror_node, "schemeURI", _dummy_dict)
        pass

    def add_hosting_facilities(self, _dict):
        """
        Adds information of the facility hosting the instrument to an ET object according to the dataCite
        schema (https://schema.datacite.org/meta/kernel-4.1/example/datacite-example-full-v4.1.xml).
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing key-value pairs
        providing information about the facility.
        :return:
        """
        if "hosting_facilities" in _dict.get_content():
            hosting_dict = ConfigDict()
            hosting_dict.read_dict(_dict.get_value("hosting_facilities"))

            _hosts = ET.SubElement(self.xml_root, "contributors")

            for _key, _value_dict in hosting_dict.get_items():
                _host_dict = ConfigDict()
                _host_dict.read_dict(_value_dict)

                if "facility_name" in _host_dict.get_content():
                    _host = ET.SubElement(_hosts, "contributor")

                    _contributor = self.add_element(
                        _host, "facility_name", _host_dict, "contributorName"
                    )
                    if "language" in _host_dict.get_content():
                        self.add_xml_lang_attribute(
                            _contributor, _host_dict.get_value("language")
                        )

                    _host_dict.add_value("contributorType", "HostingInstitution")
                    self.add_attribute(_host, "contributorType", _host_dict)

                    _host_dict.add_value("nameType", "Organizational")
                    self.add_attribute(_contributor, "nameType", _host_dict)

                    self.add_orcid_ror(
                        "facility_ROR", _host_dict, _host, "nameIdentifier", "ROR"
                    )

        pass

    def add_subjects(self, _dict):
        """
        Adds subjects that the instrument belongs to, e.g. according to the ANZSRC classification scheme
        (https://aria.stats.govt.nz/aria/#ClassificationView:uri=http://stats.govt.nz/cms/ClassificationVersion/
        d3TYSTsmz2uc8CY1).
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "abstract" whose
        value is a string.
        :return: None.
        """
        _subjects = ET.SubElement(self.xml_root, "subjects")

        subjects_dict = ConfigDict()
        subjects_dict.read_dict(_dict.get_value("subjects"))
        for _sub_no, _sub_dict in subjects_dict.get_items():
            _subject_dict = ConfigDict()
            _subject_dict.read_dict(_sub_dict)

            if "subject" in _subject_dict.get_content():
                _single_subject = self.add_element(_subjects, "subject", _subject_dict)
                for _key in _subject_dict.get_keys():
                    if not _key == "subject":
                        if _key == "language":
                            self.add_xml_lang_attribute(
                                _single_subject, _subject_dict.get_value(_key)
                            )
                        else:
                            self.add_attribute(_single_subject, _key, _subject_dict)

        pass

    def add_dates(self, _dict):
        """
        Adds various dates according to the DataCite schema.
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py).
        :return: None.
        """
        _dates = ET.SubElement(self.xml_root, "dates")

        dates_dict = ConfigDict()
        dates_dict.read_dict(_dict.get_value("dates"))

        if "start_operation" in dates_dict.get_content():
            _date_string = str(dates_dict.get_value("start_operation"))
            _date_info = "Start of instrument operation"
            if "end_operation" in dates_dict.get_content():
                _date_string += "/" + str(dates_dict.get_value("end_operation"))
                _date_info = "Period of instrument operation"

            _sub_dict = {
                "date": _date_string,
                "dateType": "Available",
                "dateInformation": _date_info,
            }
            _dummy_dict = ConfigDict()
            _dummy_dict.read_dict(_sub_dict)
            _date = self.add_element(_dates, "date", _dummy_dict)
            self.add_attribute(_date, "dateType", _dummy_dict)
            self.add_attribute(_date, "dateInformation", _dummy_dict)

        pass

    def add_description(self, _dict):
        """
        Adds a description (string text).
        :param _dict: ConfigDict (dictionary-like) class (auxiliary/config_dict.py) containing the key "abstract" whose
        value is a string.
        :return: None.
        """
        if "descriptions" in _dict.get_content():
            descriptions_dict = ConfigDict()
            descriptions_dict.read_dict(_dict.get_value("descriptions"))

            descriptions = ET.SubElement(self.xml_root, "descriptions")

            for _, _sub_dict in descriptions_dict.get_items():
                _description_dict = ConfigDict()
                _description_dict.read_dict(_sub_dict)

                if "description" in _description_dict.get_content():
                    _single_description = self.add_element(
                        descriptions, "description", _description_dict
                    )
                    if "descriptionType" in _description_dict.get_content():
                        self.add_attribute(
                            _single_description, "descriptionType", _description_dict
                        )
                    if "language" in _description_dict.get_content():
                        self.add_xml_lang_attribute(
                            _single_description, _description_dict.get_value("language")
                        )

        pass
