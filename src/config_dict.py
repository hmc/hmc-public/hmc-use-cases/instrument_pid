# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import json
import os
import sys
from pathlib import Path

import dicttoxml

path = Path(__file__)
sys.path.append(str(path.parent.absolute()))


class ConfigDict(object):
    """
    Stores and provides content of a dictionary which is usually 1) build from a json file and 2) used to configure the
    data converter.
    """

    def __init__(self):
        """
        Initializes a dictionary.
        """
        self.dictionary = dict()

        pass

    def read_dict(self, _dict):
        """
        Assigns another dictionary to self.dictionary (overwriting its content).
        :param _dict: Dictionary containing key-value pairs.
        :return: None.
        """
        self.dictionary = _dict

        pass

    def read_json(self, _path):
        """
        Routine that reads a json file to the dictionary.
        :param _path: String containing the path to a json file,
        e.g. "D:/a4-metadata/data_converter/config_project/include_data.json".
        :return: None.
        """
        json_path = Path(_path)
        if json_path.is_file():
            with open(_path, "r") as json_file:
                self.dictionary = json.load(json_file)

        pass

    def add_value(self, _key, _value):
        """
        Adds a value to the dictionary assigning it to the given key.
        :param _key: String containing the key of the dictionary to be looked for.
        :param _value: Any type which will be assigned to the _key.
        :return: None.
        """
        self.dictionary[_key] = _value

        pass

    def get_value(self, _key):
        """
        Reads the value of a key and returns that value if it exists. If value star+ts with "customClass:", the routine
        start_custom_class tries to execute a customized code of the following name which is expected to deliver the
        value to be returned.
        :param _key: String containing the key of the dictionary to be looked for.
        :return: Any type (containing the value of the key-value pair) or an empty string if key doesn't exist.
        """
        if _key in self.dictionary:
            _value = self.dictionary[_key]
        else:
            _value = ""

        return _value

    def assemble_path(self, key):
        """
        Calls the routine confirm_assemble_dict_path and abstracts the boolean to give the resembled path without
        confirmation.
        :param key: String defining the dictionary key word that contains a list of path elements.
        :return: String resembling the path.
        """
        dummy, _path = self.confirm_assemble_path(key)
        return _path

    def confirm_assemble_path(self, key):
        """
        Assembles path from list of directories and adds additional path separators where necessary.
        :param key: String defining the dictionary key word that contains a list of path elements.
        :return: Tuple of boolean (that is True if path were assembled) and string containing the path.
        """
        path_found = False
        assembled_path = ""
        if key in self.dictionary:
            path_list = self.dictionary[key]
            for i in range(0, len(path_list)):
                element = path_list[i]
                if i > 0:
                    assembled_path = os.path.join(assembled_path, element)
                else:
                    if element.endswith(":"):
                        assembled_path = os.path.join(element, os.path.sep)
                    elif "." in element:
                        # assumes it is a network address such as 'helmholtz-berlin.de'
                        assembled_path = os.path.join(
                            os.path.sep + os.path.sep, element
                        )
                    else:
                        assembled_path = element
                path_found = True

        return path_found, assembled_path

    def get_items(self):
        """
        Returns the list of key-value pairs of the dictionary - as one would get with .items().
        :return: List of key-value pairs.
        """
        return self.dictionary.items()

    def get_keys(self):
        """
        Returns the list of keys of the dictionary - as one would get with .keys().
        :return: List of dictionary keys.
        """
        return self.dictionary.keys()

    def get_lowercase_keys(self):
        """
        Returns the list of keys in lowercase of the dictionary.
        :return: List (of dictionary keys in lowercase).
        """
        low_dict = {}
        for key, value in self.dictionary.items():
            low_dict[key.lower()] = value

        return low_dict

    def get_content(self):
        """
        Returns the content of the dictionary.
        :return: Dictionary.
        """
        return self.dictionary

    def write_xml(self, _xml_path):
        """
        Writes the dictionary to a xml file.
        :param _xml_path: String containing a path to a file that should be written.
        :return: None. (Writes xml file directly.)
        """
        xml = dicttoxml(self.dictionary)
        xml_file = open(_xml_path, "w")
        xml_file.write(xml.decode("utf-8"))
        xml_file.close()

        pass
