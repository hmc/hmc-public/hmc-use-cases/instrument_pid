# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT

import os
import shutil
from pathlib import Path

import pdfkit

from src.general_aux import assemble_path, create_path
from src.xml_writer import ConfigDict, XMLWriter


class PidCreator(object):
    """
    Creates a set of files that are intended to be used for the creation of an Instrument PID, e.g. as a landing page or
    to be uploaded to a repository.
    """

    def __init__(self, project_path):
        """
        Initializes a dictionary.
        """
        self.project_path = project_path

        pass

    def create_inst_pid(self):
        """
        Starts the creation of a data set (xml, html, pdf files) to be used for an Instrument PID landing page using input
        given by 'instrument_data.json'.
        :return: None.
        """

        # read project configuration (that doesn't belong to the specific instrument)
        project_dict = ConfigDict()
        project_dict.read_json(self.project_path)

        # read instrument_meta.json file as dictionary
        default_path = os.path.join(
            project_dict.assemble_path("default_path"),
            project_dict.get_value("folder_name"),
        )
        instrument_meta_path = os.path.join(
            default_path, project_dict.get_value("instrument_meta_file")
        )
        instrument_meta_dict = ConfigDict()
        instrument_meta_dict.read_json(instrument_meta_path)

        # delete output_path to create fresh data set or create output_path if it doesn't exist
        output_path = project_dict.assemble_path("output_path")
        PidCreator.clean_path(output_path)
        create_path(output_path)

        # copy images to output_path
        PidCreator.copy_images(instrument_meta_dict, output_path)

        # create instrument xml file
        _sub_dict = instrument_meta_dict.get_value("instrument_name")
        _name_dict = ConfigDict()
        _name_dict.read_dict(_sub_dict)
        xml_filename = (
            _name_dict.get_value("instrument_name").replace(" ", "_") + ".xml"
        )

        # create xml according to DataCite
        xml_file = XMLWriter(
            "resource", instrument_meta_dict.get_content(), False, "DataCite", "1.0"
        )
        xml_file.create_landing_page()
        xml_file.write_xml(xml_filename, output_path)

        # create xml file for html and pdf file creation
        template_xml = xml_filename.replace(".xml", "_template.xml")
        landing_page = XMLWriter(
            "resource", instrument_meta_dict.get_content(), True, "DataCite", "1.0"
        )
        landing_page.create_landing_page()
        landing_page.write_xml(template_xml, output_path)

        # create html from template xml file
        html_style_sheet_path = project_dict.assemble_path("html_style_sheet")
        template_xml_path = os.path.join(output_path, template_xml)

        landing_page.xml2html(template_xml_path, html_style_sheet_path, False)

        if project_dict.get_value("create_pdf") == "True":
            # pdf creation: use different style sheet template for html and pdf creation
            pdf_style_sheet_path = project_dict.assemble_path("pdf_style_sheet")
            landing_page.xml2html(template_xml_path, pdf_style_sheet_path, True)

            html_path = os.path.join(output_path, template_xml.replace(".xml", ".html"))
            pdf_path = os.path.join(
                output_path, template_xml.replace("_template.xml", ".pdf")
            )

            wkhtml_path = project_dict.assemble_path("wkhtml_path")
            config = pdfkit.configuration(wkhtmltopdf=wkhtml_path)
            options = {"enable-local-file-access": "", "dpi": 300}
            pdfkit.from_file(
                html_path, pdf_path, configuration=config, verbose=True, options=options
            )
            # remove html template for pdf creation
            os.remove(html_path)

        # remove html template for pdf creation
        os.remove(template_xml_path)

        # release memory
        del landing_page

        pass

    @staticmethod
    def create_path(_path):
        """
        Checks if a folder exists and, if not, creates the folder.
        :param _path: String containing a (relative) folder path, e.g. 'output/A4_data_set'
        :return: None
        """
        if not os.path.isdir(_path):
            path_segments = _path.split(os.path.sep)
            current_path = ""
            first_item = True
            for item in path_segments:
                if not first_item:
                    current_path += os.path.sep
                current_path += item
                first_item = False
                if not os.path.isdir(current_path):
                    os.makedirs(current_path)
        pass

    @staticmethod
    def clean_path(_path):
        """
        (Note: Not moved to auxiliary/general_aux.py to be not available to other modules.)
        Deletes output folder (defined by _output_path) if it exists to make sure that all files within the folder were
        always freshly created.
        :param _path: String containing the path to a folder were the output should be written.
        :return: None.
        """
        if Path(_path).is_dir():
            shutil.rmtree(_path)
        else:
            create_path(_path)
        pass

    @staticmethod
    def copy_images(_instrument_dict, _output_path):
        """
        Copies image files given by key 'instrument_pictures' of _dict to the output folder.
        :param _instrument_dict: Dictionary containing the key 'instrument_pictures', e.g. the content of
        'instrument_meta.json file'.
        :param _output_path: String containing an absolute path to the output folder.
        :return: None
        """
        if "instrument_pictures" in _instrument_dict.get_content():
            for _path_list in _instrument_dict.get_value("instrument_pictures"):
                shutil.copy2(assemble_path("", _path_list), _output_path)

        pass


# standard entry point
if __name__ == "__main__":
    project_path = os.path.join("config", "project_config.json")
    instrument_pid = PidCreator(project_path)
    instrument_pid.create_inst_pid()

    pass
