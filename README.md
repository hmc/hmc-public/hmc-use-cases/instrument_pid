# Instrument PID File Creator

The Python project creates a set of files to implement an [Instrument PID](https://docs.pidinst.org/en/latest/) which would allow to unambiguously define the instrument that produces certain data. By calling

> python create_instrument_pid.py

a file set is created in the output folder which can contain

 - a xml file according to the [DataCite Metadata Schema 4.5](https://datacite-metadata-schema.readthedocs.io/en/4.5/properties/) which can be used to mint a DOI (see [here](https://docs.pidinst.org/en/latest/datacite-cookbook/minting.html#create-the-doi-using-file-upload)),
 - a html file,
 - a pdf file,
 - or a combination of the above.

and is intended to be uploaded to a globally available repository, such as [Zenodo](https://zenodo.org/). Have a look at the [output folder](./output) to see what files are produced for the A4 experiment.

See the A4 Instrument PID https://doi.org/10.15120/GSI-2024-00486 using the GSI repository for a working example.

To adapt the file set to another instrument, you have to change the content of the [config/project_config.json](./config/project_config.json) file and the corresponding instrument_meta.json file (e.g. [instruments/A4_experiment/instrument_meta.json](./instruments/A4_experiment/instrument_meta.json)).
