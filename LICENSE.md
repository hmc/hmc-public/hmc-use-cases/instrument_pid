# License

Copyright © 2024 Helmholtz-Zentrum Berlin (HZB)

This work is licensed under multiple licenses:
- The data set corresponding to the A4 experiment are subject to the license of the  [HIM](https://www.hi-mainz.de/) or [GSI](https://www.gsi.de), respectively.
- The source code and the accompanying material are licensed under [MIT](LICENSES/MIT.txt).
- The documentation and the resulting plots are licensed under [CC-BY-4.0](LICENSES/CC-BY-4.0.txt).
- Insignificant files are licensed under [CC0-1.0](LICENSES/CC0-1.0.txt).

Please see the individual files for more accurate information.
