<?xml version="1.0" encoding="UTF-8"?>
<!-- SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Berlin (HZB)
SPDX-License-Identifier: MIT -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" encoding="utf-8"/>

  <xsl:template match="/">
    <html>
      <head>
        <title>
            <xsl:for-each select="*/titles">
                <xsl:value-of select="title"/>
            </xsl:for-each>
        </title>
        <style>
            table {
                background-color: transparent;
                border: 0;
                float: left;
                margin-bottom: 50px;
                font-family: Arial;
                font-size: 10px;
                line-height: 150%;
            }
            td, th {
                text-align: left;
                vertical-align: top;
            }
            .box {
                position: relative;
                display: inline-block; /* Make the width of box same as image */
            }
            h1 {
                font-family: Arial;
                font-size: 32px;
            }
        </style>
      </head>

      <body style="margin-top: 50px">
          <table style="width: 94%;margin-left: 3%;margin-right: 3%">
              <tr>
                  <td style="border-top: 10px solid #abb8c3;padding-top:10px" colspan="2">
                    <!-- COLUMN 1: TITLE -->
                    <xsl:for-each select="*/titles">
                      <h1 style="margin-top: 20px"><xsl:value-of select="title"/></h1>
                    </xsl:for-each>
                  </td>
              </tr>

              <!-- COLUMN 1: CREATOR FACILITIES -->
              <tr>
                  <td style="text-align: right;width: 150px">
                    <b>Created by:</b>
                  </td>
                  <td>
                      <xsl:for-each select="*/creators/creator">
                        <xsl:value-of select="creatorName"/>&#160; <a href="{nameIdentifier}"><img alt="R" src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-bw-16.png" height="16" /></a>
                        <xsl:if test="position() != last()">
                          <xsl:text>, </xsl:text>
                        </xsl:if>
                      </xsl:for-each>
                  </td>
              </tr>

              <!-- HOSTING FACILITIES -->
              <tr>
                <td style="text-align: right">
                  <b>Hosted at:</b>&#160;
                </td>
                <td>
                  <xsl:for-each select="*/contributors/contributor">
                        <xsl:value-of select="contributorName"/>&#160; <a href="{nameIdentifier}"><img alt="R" src="https://raw.githubusercontent.com/ror-community/ror-logos/main/ror-icon-bw-16.png" height="16" /></a>
                    <xsl:if test="position() != last()">
                      <xsl:text>, </xsl:text>
                    </xsl:if>
                  </xsl:for-each>
                </td>
              </tr>

              <!-- OPERATION PERIOD -->
              <tr>
                <td style="text-align: right">
                  <b>In operation:</b>&#160;
                </td>
                <td>
                    <xsl:value-of select="*/dates/date"/>
                </td>
              </tr>

              <!-- DOI -->
              <tr>
                <td style="text-align: right">
                  <b>PID:</b>&#160;
                </td>
                <td>
                    <xsl:for-each select="*/identifier">
                        <a href="https://doi.org/{text()}"><xsl:value-of select="concat('https://doi.org/', text())"/></a>
                    </xsl:for-each>
                </td>
              </tr>

              <tr>
                <td colspan="2" style="padding-top: 20px;border-bottom: 10px solid #abb8c3"></td>
              </tr>
          </table>


        <!-- PART 2: description, instrument picture... -->
            <table style="width: 94%;margin-left: 3%;margin-right: 3%;margin-top: 50px">
            <!-- FIRST COLUMN -->
            <tr>
                <td>
                    <!-- ABSTRACT -->
                    <table style="width: 100%">
                        <tr>
                            <td><h2>Description</h2></td>
                        </tr>
                        <tr>
                            <td><xsl:value-of select="*/descriptions/description"/></td>
                        </tr>
                    </table>

                    <!-- REFERENCES -->
                      <table style="width: 100%">
                        <tr>
                          <td>
                            <h2>References</h2>
                          </td>
                        </tr>
                        <xsl:for-each select="*/relatedItems/relatedItem">
                          <xsl:choose>
                            <xsl:when test="@relatedItemType!='Instrument persistent identifier' and @relatedItemType!='Image'">
                              <tr>
                                <td>
                                    -&#xa;<a href="http://doi.org/{relatedItemIdentifier}"><xsl:value-of select="titles/title"/></a>
                                </td>
                                <td>
                                  <xsl:value-of select="@description"/>
                                </td>
                              </tr>
                            </xsl:when>
                          </xsl:choose>
                        </xsl:for-each>
                      </table>
                </td>

              <!-- SECOND COLUM: IMAGES -->
    <!--            <td style="width:45%">-->
                <td style="width: 45%;margin-top: 15px;padding-left: 3%">
                    <table style="width: 100%">
                      <xsl:for-each select="*/relatedItems/relatedItem">
                        <xsl:choose>
                          <xsl:when test="@relatedItemType='Image'">
                              <xsl:for-each select="titles/title">
                              <tr>
                                <td style="padding-top: 10px;padding-bottom: 25px">
                                  <div class="box">
                                    <img alt="A4 experiment" src="{text()}" width="100%"/>
                                  </div>
                                </td>
                              </tr>
                               </xsl:for-each>
                          </xsl:when>
                        </xsl:choose>
                      </xsl:for-each>
                    </table>
                </td>
            </tr>
          </table>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
